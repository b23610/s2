package com.zuitt.example;

import java.util.ArrayList;

public class S2A2 {
    public static void main(String[] args){
        // prime numbers array
        int[] primeNumbersArray = new int[5];
        primeNumbersArray[0] = 2;
        primeNumbersArray[1] = 3;
        primeNumbersArray[2] = 5;
        primeNumbersArray[3] = 7;
        primeNumbersArray[4] = 13;

        System.out.println("The first prime number is: " + primeNumbersArray[0]);
        System.out.println("The second prime number is: " + primeNumbersArray[1]);
        System.out.println("The third prime number is: " + primeNumbersArray[2]);
        System.out.println("The fourth prime number is: " + primeNumbersArray[3]);
        System.out.println("The fifth prime number is: " + primeNumbersArray[4]);

        // string array
        ArrayList<String> friends = new ArrayList<String>();
        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");
        System.out.println("My friends are: " + friends);
    }
}
