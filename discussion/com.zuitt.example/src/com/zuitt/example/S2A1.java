package com.zuitt.example;

import java.util.Scanner;

public class S2A1 {
    public static void main(String[] args) {
        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Let's check if the year is a leap year. Enter a year: ");
        int year = numberScanner.nextInt();

        if (year%4 ==0 || (year%400 ==0)) {
            System.out.println("The year" + year + " is a leap year");
        } else {
            System.out.println("The year" + year + " is not a leap year");
        }
    }
}
